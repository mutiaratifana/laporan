**4. Node.js**
<p>Node.js is an open source server environment.
Node.js allows you to run JavaScript on the server.</p>

**learn Node.js**
1. Make sure you have installed node.js from nodejs.org
2. Set the path environment
3. Try this simple example
```module
exports.myDateTime = function () {
    return Date();
};
```
Use the exports keyword to make properties and methods available outside the module file.
Save the code above in a file called "myfirstmodule.js"

```nodejs
var http = require('http');

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World!');
}).listen(8080,'127.0.0.1');
```
and when you go to '127.0.0.1:8080' on your website, the message will appear.
