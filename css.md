**2. CSS3**
*<p>What is CSS?</p>*
CSS (Cascading Style Sheets) is a language that describes the style of an HTML document. CSS describes how HTML elements should be displayed.

<p>This is an example for CSS syntax</p>


```css
    <!DOCTYPE html>
<html>
<head>
<style>
body {
    background-color: blue;
}

h1 {
    color: white;
    text-align: center;
}
</style>
</head>
<body>

<h1>My First CSS Example</h1>
</body>
</html>
```
The output of the syntax above is to change the background color to blue, the words of the h1 will be white for the color, and text align at the center.