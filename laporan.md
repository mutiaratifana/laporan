### TRAINEE 2018

**1. HTML5**<br>
<p> HTML5 is the latest version of Hypertext Markup Language, the code that describes web pages.</p>
With HTML, you can create your own website.<br>
This is an example for HTML.

```html
<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
</head>
<body>
<h1>This is a Heading</h1>
<p>This is a paragraph.</p>
</body>
</html>
```
<p> Here is the output..</p>
<h1>This is a Heading</h1>
<p>This is a paragraph.</p>

**2. CSS3**
*<p>What is CSS?</p>*
CSS (Cascading Style Sheets) is a language that describes the style of an HTML document. CSS describes how HTML elements should be displayed.

<p>This is an example for CSS syntax</p>


```css
    <!DOCTYPE html>
<html>
<head>
<style>
body {
    background-color: blue;
}

h1 {
    color: white;
    text-align: center;
}
</style>
</head>
<body>

<h1>My First CSS Example</h1>
</body>
</html>
```
The output of the syntax above is to change the background color to blue, the words of the h1 will be white for the color, and text align at the center.

**3. Javascript**
<h2>Why Study Javascript?</h1>
<p>JavaScript is one of the 3 languages all web developers must learn:

   1. HTML to define the content of web pages

   2. CSS to specify the layout of web pages

   3. JavaScript to program the behavior of web pages

Web pages are not the only place where JavaScript is used. Many desktop and server programs use JavaScript. Node.js is the best known. Some databases, like MongoDB and CouchDB, also use JavaScript as their programming language.</p>
```javascript
<!DOCTYPE html>
<html>
<body>

<h2>My First JavaScript</h2>

<button type="button"
onclick="document.getElementById('demo').innerHTML = Date()">
Click me to display Date and Time.</button>

<p id="demo"></p>
<p>
</body>
</html> 

```
Here is the output of the syntax above. <br>

![](./image/Capture.png)
"Click me to display Date and Time" is a button which is if you click the button, the date will appear.

**4. Node.js**
<p>Node.js is an open source server environment.
Node.js allows you to run JavaScript on the server.</p>

**learn Node.js**
1. Make sure you have installed node.js from nodejs.org
2. Set the path environment
3. Try this simple example
```module
exports.myDateTime = function () {
    return Date();
};
```
Use the exports keyword to make properties and methods available outside the module file.
Save the code above in a file called "myfirstmodule.js"

```nodejs
var http = require('http');

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World!');
}).listen(8080,'127.0.0.1');
```
and when you go to '127.0.0.1:8080' on your website, the message will appear.

