### TRAINEE 2018

**1. HTML5**<br>
<p> HTML5 is the latest version of Hypertext Markup Language, the code that describes web pages.</p>
With HTML, you can create your own website.<br>
This is an example for HTML.

```html
<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
</head>
<body>
<h1>This is a Heading</h1>
<p>This is a paragraph.</p>
</body>
</html>
```
<p> Here is the output..</p>
<h1>This is a Heading</h1>
<p>This is a paragraph.</p>
