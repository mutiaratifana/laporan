**3. Javascript**
<h2>Why Study Javascript?</h1>
<p>JavaScript is one of the 3 languages all web developers must learn:

   1. HTML to define the content of web pages

   2. CSS to specify the layout of web pages

   3. JavaScript to program the behavior of web pages

Web pages are not the only place where JavaScript is used. Many desktop and server programs use JavaScript. Node.js is the best known. Some databases, like MongoDB and CouchDB, also use JavaScript as their programming language.</p>
```javascript
<!DOCTYPE html>
<html>
<body>

<h2>My First JavaScript</h2>

<button type="button"
onclick="document.getElementById('demo').innerHTML = Date()">
Click me to display Date and Time.</button>

<p id="demo"></p>
<p>
</body>
</html> 

```
Here is the output of the syntax above. <br>

![](./image/Capture.png)
"Click me to display Date and Time" is a button which is if you click the button, the date will appear.